#include "TComTimer.h"


TComTimer::TComTimer(TimerResolution tResol)
{
	tResolution=tResol;
	QueryPerformanceFrequency(&frequency);
}


TComTimer::~TComTimer(void)
{
}

bool TComTimer::startTimer(void)		//record start time
{
	return(QueryPerformanceCounter(&startTime));
}

bool TComTimer::stopTimer(void)		//record end time
{
	return(QueryPerformanceCounter(&endTime));
}

void TComTimer::calcTime()
{
	LONGLONG timeDiff=(endTime.QuadPart-startTime.QuadPart);
	long double divisor;
	switch(tResolution)
	{
	case MILLI_SEC:
		divisor=(long double)(frequency.QuadPart/1000);
			break;
	case MICRO_SEC:
		divisor=(long double)(frequency.QuadPart/1000000);
			break;
	case NANO_SEC:
		divisor=(long double)(frequency.QuadPart/1000000000);
			break;
	default:
		printf("\nERROR:invalid timer resolution parameter");
		return;
			break;
	}
	tDiff=(long double)(timeDiff)/(divisor);
}

void TComTimer::dump(char *str)
{
	printf("\n%s : %lf",str,tDiff);
}