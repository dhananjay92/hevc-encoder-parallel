#pragma once

#include <Windows.h>
#include <stdio.h>
enum TimerResolution { MILLI_SEC=0, MICRO_SEC=1, NANO_SEC=2 };


//high resolution timer for windows
class TComTimer		
{
	LARGE_INTEGER startTime;
	LARGE_INTEGER endTime;
	LARGE_INTEGER frequency;
	long double tDiff;
	TimerResolution tResolution;
public:
	TComTimer(TimerResolution tResol);
	~TComTimer(void);

	bool startTimer(void);		//record start time

	bool stopTimer(void);		//record end time

	void calcTime(void);		//calculate time difference

	void dump(char *str);			//dump time difference on screen
};

